TITLE CS2810 Assembler Assigmnet #2

; Student Name: Garrick McMickell
; Assignment Due Date: 4/10/16

INCLUDE Irvine32.inc
.data
	vSemester byte "CS2810 Spring Semester 2016", 0
	vAssignment byte "Assembler Assignment #2", 0
	vName byte "Garrick McMickell", 0
	vPrompt byte "Enter FAT16 file time in HEX format: ", 0
	vTime byte "--:--:--", 0

.code
main PROC
	call clrscr

	mov dh, 7
	mov dl, 0
	call gotoxy

	mov edx, offset vSemester
	call writestring

	mov dh, 8
	mov dl, 0
	call gotoxy

	mov edx, offset vAssignment
	call writestring

	mov dh, 9
	mov dl, 0
	call gotoxy

	mov edx, offset vName
	call writestring

	mov dh, 11
	mov dl, 0
	call gotoxy

	mov edx, offset vPrompt
	call writestring
	
	call readhex

	ror ax, 8
	mov ecx, eax

	and ax, 1111100000000000b
	shr ax, 11
	mov bh, 10
	div bh
	add ax, 3030h

	mov word ptr [vTime + 0], ax

	mov eax, ecx

	and ax, 0000011111100000b
	shr ax, 5
	mov bh, 10
	div bh
	add ax, 3030h

	mov word ptr [vTime + 3], ax

	mov eax, ecx

	and ax, 0000000000011111b
	shl ax, 1
	mov bh, 10
	div bh
	add ax, 3030h

	mov word ptr [vTime + 6], ax

	mov dh, 12
	mov dl, 0
	call gotoxy

	mov edx, offset vTime
	call writestring

	xor ecx, ecx
	call readchar

	exit
main ENDP

END main